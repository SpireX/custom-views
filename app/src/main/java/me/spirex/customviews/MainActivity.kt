package me.spirex.customviews

import android.app.Activity
import android.os.Bundle
import kotlinx.android.synthetic.main.main_activity.*
import me.spirex.customviews.seekbar.SeekBarBounded

class MainActivity: Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        this.MainActivity_SeekBar.setOnSeekBarChangeListener(object: SeekBarBounded.OnSeekBarChangeListener{
            override fun onOnSeekBarValueChange(bar: SeekBarBounded, value: Double) {
                this@MainActivity.MainActivity_Value.text = "Value: $value"
            }
        })
    }
}