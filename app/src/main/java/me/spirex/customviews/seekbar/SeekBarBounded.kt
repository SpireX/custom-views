package me.spirex.customviews.seekbar

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import me.spirex.customviews.R


class SeekBarBounded : View {
    companion object {
        private val DEFAULT_RANGE_COLOR = Color.argb(0xFF, 0x33, 0xB5, 0xE5)
        private const val DEFAULT_BACKGROUND_COLOR = Color.GRAY
        private const val DEFAULT_MIN_VALUE = 0f
        private const val DEFAULT_MAX_VALUE = 100f
        private val DEFAULT_MIN_BOUND = Float.MIN_VALUE
        private val DEFAULT_MAX_BOUND = Float.MAX_VALUE
        private const val DEFAULT_BOUND_STROKE_WIDTH = 2f

        /**
         * An invalid pointer id.
         */
        const val INVALID_POINTER_ID = 255

        // Localized constants from MotionEvent for compatibility
        // with API < 8 "Froyo".
        const val ACTION_POINTER_UP = 0x6
        const val ACTION_POINTER_INDEX_MASK = 0x0000ff00
        const val ACTION_POINTER_INDEX_SHIFT = 8
    }

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var absoluteMinValue: Double = 0.0
    private var absoluteMaxValue: Double = 0.0
    private var absoluteMinBound: Double = 0.0
    private var absoluteMaxBound: Double = 0.0
    private lateinit var thumbImage: Drawable
    private lateinit var thumbPressedImage: Drawable
    private var defaultRangeColor: Int = Color.TRANSPARENT
    private var defaultBackgroundColor: Int = Color.TRANSPARENT
    private var showBounds: Boolean = true
    private var boundStrokeWidth: Float = DEFAULT_BOUND_STROKE_WIDTH

    private var thumbWidth: Float = 0f
    private var thumbHalfWidth: Float = 0f
    private var thumbHalfHeight: Float = 0f
    private var lineHeight: Float = 0f
    private var padding: Float = 0f
    private var scaledTouchSlop: Int = 1
    private var isDragging: Boolean = false
    private var isThumbPressed: Boolean = false
    private var normalizedThumbValue = 0.0
    private val notifyWhileDragging = true
    private var listener: OnSeekBarChangeListener? = null


    constructor(context: Context) : super(context) {
        onInit(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        onInit(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        onInit(context, attrs, defStyleAttr)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        onInit(context, attrs, defStyleAttr)
    }

    /**
     * Callback listener interface to notify about changed range values.
     *
     */
    interface OnSeekBarChangeListener {
        fun onOnSeekBarValueChange(
            bar: SeekBarBounded,
            value: Double
        )
    }

    /**
     * Registers given listener callback to notify about changed selected
     * values.
     *
     * @param listener The listener to notify about changed selected values.
     */
    fun setOnSeekBarChangeListener(
        listener: OnSeekBarChangeListener
    ) {
        this.listener = listener
    }

    private var mDownMotionX: Float = 0.toFloat()
    private var mActivePointerId = INVALID_POINTER_ID


    private fun onInit(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {

        // Attribute initialization
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.SeekBarBounded,
            defStyleAttr, 0
        )

        var thumbImageDrawable = a.getDrawable(R.styleable.SeekBarBounded_ThumbDrawable)
        if (thumbImageDrawable == null) {
            thumbImageDrawable = resources.getDrawable(R.drawable.ic_thumb)
        }
        this.thumbImage = thumbImageDrawable

        var thumbImagePressedDrawable = a.getDrawable(R.styleable.SeekBarBounded_ThumbPressedDrawable)
        if (thumbImagePressedDrawable == null) {
            thumbImagePressedDrawable = resources.getDrawable(R.drawable.ic_thumb)
        }
        this.thumbPressedImage = thumbImagePressedDrawable
        this.absoluteMinValue = a.getFloat(R.styleable.SeekBarBounded_MinValue, DEFAULT_MIN_VALUE).toDouble()
        this.absoluteMaxValue = a.getFloat(R.styleable.SeekBarBounded_MaxValue, DEFAULT_MAX_VALUE).toDouble()
        this.absoluteMinBound = a.getFloat(R.styleable.SeekBarBounded_MinBound, DEFAULT_MIN_BOUND).toDouble()
        this.absoluteMaxBound = a.getFloat(R.styleable.SeekBarBounded_MaxBound, DEFAULT_MAX_BOUND).toDouble()
        this.boundStrokeWidth = a.getDimension(R.styleable.SeekBarBounded_BoundStrokeWidth, DEFAULT_BOUND_STROKE_WIDTH)
        this.showBounds = a.getBoolean(R.styleable.SeekBarBounded_ShowBounds, true)

        this.defaultBackgroundColor = a.getColor(
            R.styleable.SeekBarBounded_BackgroundColor,
            DEFAULT_BACKGROUND_COLOR
        )
        this.defaultRangeColor = a.getColor(
            R.styleable.SeekBarBounded_FillColor,
            DEFAULT_RANGE_COLOR
        )


        thumbWidth = thumbImage.intrinsicWidth.toFloat()
        thumbHalfWidth = 0.5f * thumbWidth
        thumbHalfHeight = 0.5f * thumbImage.intrinsicHeight
        lineHeight = a.getDimension(R.styleable.SeekBarBounded_LineStrokeWidth, 0.3f * thumbHalfWidth)
        a.recycle()

        padding = thumbHalfWidth
        isFocusable = true
        isFocusableInTouchMode = true
        scaledTouchSlop = ViewConfiguration.get(getContext())
            .scaledTouchSlop
        setAbsoluteMinMaxBounds(this.absoluteMinBound, this.absoluteMaxBound)
    }

    fun setAbsoluteMinMaxValue(absoluteMinValue: Double, absoluteMaxValue: Double) {
        this.absoluteMinValue = absoluteMinValue
        this.absoluteMaxValue = absoluteMaxValue
        if (absoluteMinValue > normalizedThumbValue) {
            normalizedThumbValue = valueToNormalized(absoluteMinValue)
        }
        if (absoluteMaxValue < normalizedThumbValue) {
            normalizedThumbValue = valueToNormalized(absoluteMaxValue)
        }
    }

    fun setAbsoluteMinMaxBounds(minBound: Double, maxBound: Double) {
        this.absoluteMinBound = valueToNormalized(minBound)
        this.absoluteMaxBound = valueToNormalized(maxBound)
        if (absoluteMinValue < absoluteMinBound && normalizedThumbValue < absoluteMinBound) {
            normalizedThumbValue = absoluteMinBound
        }
        if (absoluteMaxValue > absoluteMaxBound && normalizedThumbValue > absoluteMaxBound) {
            normalizedThumbValue = absoluteMaxBound
        }
    }

    @Synchronized
    override fun onMeasure(
        widthMeasureSpec: Int,
        heightMeasureSpec: Int
    ) {
        var width = 200
        if (View.MeasureSpec.UNSPECIFIED != View.MeasureSpec.getMode(widthMeasureSpec)) {
            width = View.MeasureSpec.getSize(widthMeasureSpec)
        }
        var height = thumbImage.intrinsicHeight
        if (View.MeasureSpec.UNSPECIFIED != View.MeasureSpec.getMode(heightMeasureSpec)) {
            height = Math.min(height, View.MeasureSpec.getSize(heightMeasureSpec))
        }
        setMeasuredDimension(width, height)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled)
            return false

        val pointerIndex: Int
        when (event.action and MotionEvent.ACTION_MASK) {

            MotionEvent.ACTION_DOWN -> {
                // Remember where the motion event started
                mActivePointerId = event.getPointerId(event.pointerCount - 1)
                pointerIndex = event.findPointerIndex(mActivePointerId)
                mDownMotionX = event.getX(pointerIndex)

                isThumbPressed = true //evalPressedThumb(mDownMotionX)


                isPressed = true
                invalidate()
                onStartTrackingTouch()
                trackTouchEvent(event)
                attemptClaimDrag()
            }
            MotionEvent.ACTION_MOVE -> if (isThumbPressed) {

                if (isDragging) {
                    trackTouchEvent(event)
                } else {
                    // Scroll to follow the motion event
                    pointerIndex = event.findPointerIndex(mActivePointerId)
                    val x = event.getX(pointerIndex)

                    if (Math.abs(x - mDownMotionX) > scaledTouchSlop) {
                        isPressed = true
                        invalidate()
                        onStartTrackingTouch()
                        trackTouchEvent(event)
                        attemptClaimDrag()
                    }
                }

                if (notifyWhileDragging && listener != null) {
                    listener!!.onOnSeekBarValueChange(this, normalizedToValue(normalizedThumbValue))
                }
            }
            MotionEvent.ACTION_UP -> {
                if (isDragging) {
                    trackTouchEvent(event)
                    onStopTrackingTouch()
                    isPressed = false
                } else {
                    // Touch up when we never crossed the touch slop threshold
                    // should be interpreted as a tap-seek to that location.
                    onStartTrackingTouch()
                    trackTouchEvent(event)
                    onStopTrackingTouch()
                }

                isThumbPressed = false
                invalidate()
                if (listener != null) {
                    listener!!.onOnSeekBarValueChange(this, normalizedToValue(normalizedThumbValue))
                }
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                val index = event.pointerCount - 1
                // final int index = ev.getActionIndex();
                mDownMotionX = event.getX(index)
                mActivePointerId = event.getPointerId(index)
                invalidate()
            }
            MotionEvent.ACTION_POINTER_UP -> {
                onSecondaryPointerUp(event)
                invalidate()
            }
            MotionEvent.ACTION_CANCEL -> {
                if (isDragging) {
                    onStopTrackingTouch()
                    isPressed = false
                }
                invalidate() // see above explanation
            }
        }
        return true
    }

    private fun onSecondaryPointerUp(ev: MotionEvent) {
        val pointerIndex = ev.action and ACTION_POINTER_INDEX_MASK shr ACTION_POINTER_INDEX_SHIFT

        val pointerId = ev.getPointerId(pointerIndex)
        if (pointerId == mActivePointerId) {
            // This was our active pointer going up. Choose
            // a new active pointer and adjust accordingly.
            // TODO: Make this decision more intelligent.
            val newPointerIndex = if (pointerIndex == 0) 1 else 0
            mDownMotionX = ev.getX(newPointerIndex)
            mActivePointerId = ev.getPointerId(newPointerIndex)
        }
    }

    /**
     * Tries to claim the user's drag motion, and requests disallowing any
     * ancestors from stealing events in the drag.
     */
    private fun attemptClaimDrag() {
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true)
        }
    }

    private fun trackTouchEvent(event: MotionEvent) {
        val pointerIndex = event.findPointerIndex(mActivePointerId)
        val x = event.getX(pointerIndex)
        setNormalizedValue(screenToNormalized(x))
    }

    /**
     * Converts screen space x-coordinates into normalized values.
     *
     * @param screenCoord The x-coordinate in screen space to convert.
     * @return The normalized value.
     */
    private fun screenToNormalized(screenCoord: Float): Double {
        val width = width
        if (width <= 2 * padding) {
            // prevent division by zero, simply return 0.
            return 0.0
        } else {
            val result = ((screenCoord - padding) / (width - 2 * padding)).toDouble()
            return Math.min(1.0, Math.max(0.0, result))
        }
    }

    /**
     * Converts a normalized value to a Number object in the value space between
     * absolute minimum and maximum.
     *
     * @param normalized
     * @return
     */
    private fun normalizedToValue(normalized: Double): Double {
        return absoluteMinValue + normalized * (absoluteMaxValue - absoluteMinValue)
    }

    /**
     * Converts the given Number value to a normalized double.
     *
     * @param value The Number value to normalize.
     * @return The normalized double.
     */

    private fun valueToNormalized(value: Double): Double {
        return if (0.0 == absoluteMaxValue - absoluteMinValue) {
            // prevent division by zero, simply return 0.
            0.0
        } else (value - absoluteMinValue) / (absoluteMaxValue - absoluteMinValue)
    }


    /**
     * Sets normalized max value to value so that 0 <= normalized min value <=
     * value <= 1. The View will get invalidated when calling this method.
     *
     * @param value The new normalized max value to set.
     */

    private fun setNormalizedValue(value: Double) {
        normalizedThumbValue = Math.max(0.0, value)
        if (absoluteMinValue < absoluteMinBound && normalizedThumbValue < absoluteMinBound) {
            normalizedThumbValue = absoluteMinBound
        }
        if (absoluteMaxValue > absoluteMaxBound && normalizedThumbValue > absoluteMaxBound) {
            normalizedThumbValue = absoluteMaxBound
        }
        invalidate()
    }

    var value: Double
        get() = normalizedToValue(normalizedThumbValue)
        set(value) = setProgress(value)

    /**
     * Sets value of seekbar to the given value
     * @param value The new value to set
     */
    private fun setProgress(value: Double) {
        val newThumbValue = valueToNormalized(value)
        if (newThumbValue > absoluteMaxValue || newThumbValue < absoluteMinValue) {
            throw IllegalArgumentException("Value should be in the middle of max and min value")
        }
        normalizedThumbValue = newThumbValue
        invalidate()
    }

    /**
     * This is called when the user has started touching this widget.
     */
    fun onStartTrackingTouch() {
        isDragging = true
    }

    /**
     * This is called when the user either releases his touch or the touch is
     * canceled.
     */
    fun onStopTrackingTouch() {
        isDragging = false
    }

    /**
     * Decides which (if any) thumb is touched by the given x-coordinate.
     *
     * @param touchX The x-coordinate of a touch event in screen space.
     * @return The pressed thumb or null if none has been touched.
     */
    private fun evalPressedThumb(touchX: Float): Boolean {
        return isInThumbRange(touchX, normalizedThumbValue)
    }

    /**
     * Decides if given x-coordinate in screen space needs to be interpreted as
     * "within" the normalized thumb x-coordinate.
     *
     * @param touchX               The x-coordinate in screen space to check.
     * @param normalizedThumbValue The normalized x-coordinate of the thumb to check.
     * @return true if x-coordinate is in thumb range, false otherwise.
     */
    private fun isInThumbRange(touchX: Float, normalizedThumbValue: Double): Boolean {
        return Math.abs(touchX - normalizedToScreen(normalizedThumbValue)) <= thumbHalfWidth
    }

    /**
     * Converts a normalized value into screen space.
     *
     * @param normalizedCoord The normalized value to convert.
     * @return The converted value in screen space.
     */
    private fun normalizedToScreen(normalizedCoord: Double): Float {
        return (padding + normalizedCoord * (width - 2 * padding)).toFloat()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // draw seek bar background line
        val rect = RectF(
            padding,
            0.5f * (height - lineHeight), width - padding,
            0.5f * (height + lineHeight)
        )
        paint.color = defaultBackgroundColor
        canvas.drawRect(rect, paint)

        rect.left = normalizedToScreen(normalizedThumbValue)
        paint.color = defaultRangeColor
        canvas.drawRect(rect, paint)

        if (showBounds) {
            var boundX: Float
            paint.strokeWidth = boundStrokeWidth
            if (absoluteMinValue < absoluteMinBound) {
                paint.color = defaultBackgroundColor
                boundX = normalizedToScreen(absoluteMinBound)
                canvas.drawLine(boundX, rect.top - 5, boundX, rect.bottom + 5, paint)
            }

            if (absoluteMaxValue > absoluteMaxBound) {
                paint.color = defaultRangeColor
                boundX = normalizedToScreen(absoluteMaxBound)
                canvas.drawLine(boundX, rect.top - 5, boundX, rect.bottom + 5, paint)
            }
        }

        drawThumb(
            normalizedToScreen(normalizedThumbValue),
            isThumbPressed, canvas
        )
    }

    /**
     * Draws the "normal" resp. "pressed" thumb image on specified x-coordinate.
     *
     * @param screenCoord The x-coordinate in screen space where to draw the image.
     * @param pressed     Is the thumb currently in "pressed" state?
     * @param canvas      The canvas to draw upon.
     */
    private fun drawThumb(screenCoord: Float, pressed: Boolean, canvas: Canvas) {
        val drawable = if (pressed) {
            thumbPressedImage
        } else {
            thumbImage
        }
        drawable.setBounds(
            (screenCoord - thumbHalfWidth).toInt(),
            (0.5f * height - thumbHalfHeight).toInt(),
            (screenCoord + thumbHalfWidth).toInt(),
            (0.5f * height + thumbHalfHeight).toInt()
        )
        drawable.draw(canvas)
    }
}